pragma solidity >=0.4.21 <0.6.0;
import "../utils/TokenClaimer.sol";
import "../utils/Ownable.sol";
import "../utils/SafeMath.sol";
import "../erc20/IERC20.sol";
import "../erc20/ERC20Impl.sol";
import "../utils/Address.sol";
import "../utils/ReentrancyGuard.sol";
import "../erc20/SafeERC20.sol";
import "./Interfaces.sol";


contract TradeInterface{
   function get_total_chip() public view returns(uint256);

}

contract Entry is Ownable, ReentrancyGuard{
    using SafeMath for uint256;
    using SafeERC20 for IERC20;
    address public stable_token;
    address public chip;
    address public fee_pool;
    TradeInterface public vtrade;
    uint256 public min_amount;
    uint256 public fee_rate;//base 10000
    constructor(address _stable_token, address _chip, address _vtrade) public{
        stable_token = _stable_token;
        chip = _chip;
        vtrade = TradeInterface(_vtrade);
        fee_rate = 200;
    }

    event PUSDDeposit(uint256 stable_amount, uint256 chip_amount);
    function deposit(uint256 _amount) public nonReentrant{
        require(_amount >= min_amount, "too small amount");
        uint before = IERC20(stable_token).balanceOf(address(this));
        IERC20(stable_token).safeTransferFrom(msg.sender, address(this), _amount);
        uint _after = IERC20(stable_token).balanceOf(address(this));
        uint amount = _after.safeSub(before);
        uint chip_amount;
        if (before == 0){
            chip_amount = amount;
        }
        else{
            chip_amount = vtrade.get_total_chip().safeMul(amount).safeDiv(before);
        }
        TokenInterface(chip).generateTokens(msg.sender, chip_amount);
        emit PUSDDeposit(amount, chip_amount);
    }
    event PUSDWithdraw(uint256 stable_amount, uint256 chip_amount);
    function withdraw(uint256 _amount) public nonReentrant{
        require(IERC20(chip).balanceOf(msg.sender) >= _amount, "not enough chip");
        uint256 total_chip = TradeInterface(vtrade).get_total_chip();
        uint256 stable_amount = _amount.safeMul(IERC20(stable_token).balanceOf(address(this))).safeDiv(total_chip);
        require(stable_amount <= IERC20(stable_token).balanceOf(address(this)), "error: not enough pusd supply");
        TokenInterface(chip).destroyTokens(msg.sender, _amount);

        if ((fee_pool == address(0)) || fee_rate == 0){
          IERC20(stable_token).safeTransfer(msg.sender, stable_amount);}
        else {
          uint256 f = stable_amount.safeMul(fee_rate).safeDiv(10000);
          IERC20(stable_token).safeTransfer(fee_pool, f);
          IERC20(stable_token).safeTransfer(msg.sender, stable_amount.safeSub(f));  
        }
        emit PUSDWithdraw(stable_amount, _amount);
    }
    event ChangeMinPUSDAmount(uint256 new_amount);
    function change_min_amount(uint256 _amount) public onlyOwner{
        min_amount = _amount;
        emit ChangeMinPUSDAmount(_amount);
    }
    event NewVirtualTrade(address virtual_trade);
    function change_virtual_trade(address _vtrade) public onlyOwner{
        require(vtrade.get_total_chip() == 0, "withdraw all assets first");
        vtrade = TradeInterface(_vtrade);   
        emit NewVirtualTrade(_vtrade);
    }
    event NewFeePool(address addr);
    function change_fee_pool(address _fee_pool) public onlyOwner{
        fee_pool = _fee_pool;
        emit NewFeePool(fee_pool);
    }

    event NewFeeRate(uint256 rate);
    function change_fee_rate(uint256 _fee_rate) public onlyOwner{
        fee_rate = _fee_rate;
        emit NewFeeRate(fee_rate);
    }
}

contract EntryFactory{
  event CreateEntry(address addr);
  function newEntry(address _stable_token, address _chip, address _vtrade) public returns(address){
    Entry vt = new Entry(_stable_token, _chip, _vtrade);
    emit CreateEntry(address(vt));
    vt.transferOwnership(msg.sender);
    return address(vt);
  }
}
