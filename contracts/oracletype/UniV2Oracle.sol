pragma solidity >=0.4.21 <0.6.0;
import "../utils/SafeMath.sol";
import "../erc20/ERC20Impl.sol";

contract UniV2Interface{
  function getReserves() public view returns (uint112 _reserve0, uint112 _reserve1, uint32 _blockTimestampLast);
  address public token0;
  address public token1;
}

contract UniV2OracleType{
  using SafeMath for uint256;
  string public name; 
  constructor() public{
    name = "UniV2 Oracle Type";
  }
  function get_asset_price(address addr, bool b) public view returns(uint256){
      (uint112 r0, uint112 r1, ) = UniV2Interface(addr).getReserves();
      address t0 = UniV2Interface(addr).token0();
      address t1 = UniV2Interface(addr).token1();

      if (b){//r0 is stable token
        return uint256(r0).safeMul(1e18).safeDiv(uint256(10)**ERC20Base(t0).decimals()).safeMul(uint256(10)**ERC20Base(t1).decimals()).safeDiv(uint256(r1));
      }
      else{//r1 is stable token
        return uint256(r1).safeMul(1e18).safeDiv(uint256(10)**ERC20Base(t1).decimals()).safeMul(uint256(10)**ERC20Base(t0).decimals()).safeDiv(uint256(r0));
      }
  }

  function getPriceDecimal() public pure returns(uint256){
    return 1e18;
  }
}

contract UniV2OracleTypeFactory {
  event CreateUniV2OracleType(address addr);

  function newUniV2OracleType() public returns(address){
    UniV2OracleType vt = new UniV2OracleType();
    emit CreateUniV2OracleType(address(vt));
    return address(vt);
  }
}