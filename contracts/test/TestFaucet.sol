pragma solidity >=0.4.21 <0.6.0;
import "../core/Interfaces.sol";

contract TestFaucet{
    address pusd;
    address plut;

    constructor() public{
        pusd = 0x59a9bda10d60dD893eE1089AE0782B41BE1b1652;
        plut = 0xA0B2ec6B157B282281D76D42a667f5D837f8AF2A;
    }

    function getPUSD() public{
        TokenInterface(pusd).generateTokens(msg.sender, 10000000000000000000000);
    }
    function getPLUT() public{
        TokenInterface(plut).generateTokens(msg.sender, 10000000000000000000000);
    }
}