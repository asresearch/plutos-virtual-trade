const AddressArray = artifacts.require("AddressArray");
const fs = require("fs");
const {DHelper, StepRecorder} = require("../util.js");
const SafeMath = artifacts.require("SafeMath");
const SafeERC20 = artifacts.require("SafeERC20");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const VirtualTradeFactory = artifacts.require("VirtualTradeFactory");
const VirtualTrade = artifacts.require("VirtualTrade");
const OracleFactory = artifacts.require("OracleFactory");
const Oracle = artifacts.require("Oracle");
const EntryFactory = artifacts.require("EntryFactory");
const Entry = artifacts.require("Entry");
const ChainlinkOracleTypeFactory = artifacts.require("ChainlinkOracleTypeFactory")
const ChainlinkOracleType = artifacts.require("ChainlinkOracleType")






async function performMigration(deployer, network, accounts, dhelper) {
    sr = StepRecorder(network, "virtual-trade");

    console.log("creating trustlist...")
    tlist_factory = await TrustListFactory.deployed();
    tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
    token_trustlist = await TrustList.at(tx.logs[0].args.addr);
    token_trustlist.add_trusted(accounts[0]) //change this in main
    sr.write("chip_tl", token_trustlist.address)

    tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
    oracle_trustlist = await TrustList.at(tx.logs[0].args.addr);
    sr.write("oracle_tl", oracle_trustlist.address)

    pusd = "0x3dfe621515206b583D5985Bb6F891E5F43868e73"

    console.log("creating chip...")

    token_factory = await ERC20TokenFactory.deployed();
    tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000', 0, "plutos_chip", 18, "CHIP", true);
    chip = await ERC20Token.at(tx.logs[0].args._cloneToken);
    sr.write("chip", chip.address)
    await chip.changeTrustList(token_trustlist.address);

    cl_factory = await ChainlinkOracleTypeFactory.deployed();
    tx = await cl_factory.newChainlinkOracleType();
    cloracletype = await ChainlinkOracleType.at(tx.logs[0].args.addr);
    sr.write("chainlink oracle type", cloracletype.address)

    /*if (network.includes("ropsten")){
      oracle = await Oracle.at("0xda980D1E2C78e482d864804cE1c3F6177c4C261B")
    }
    else{*/
    console.log("creating oracle...")

    oracle_factory = await OracleFactory.deployed();
    tx = await oracle_factory.newOracle();
    oracle = await Oracle.at(tx.logs[0].args.addr);    
    sr.write("oracle", oracle.address)
    await oracle.changeTrustList(oracle_trustlist.address);
    //}

    console.log("creating virtual trade...")
    vt_factory = await VirtualTradeFactory.deployed();
    tx = await vt_factory.newVirtualTrade(chip.address, oracle.address);
    vtrade = await VirtualTrade.at(tx.logs[0].args.addr);
    sr.write("virtualtrade", vtrade.address);
    await oracle_trustlist.add_trusted(vtrade.address)
    await token_trustlist.add_trusted(vtrade.address);

    console.log("creating entry...")

    entry_factory = await EntryFactory.deployed();
    tx = await entry_factory.newEntry(pusd, chip.address, vtrade.address);
    entry = await Entry.at(tx.logs[0].args.addr);
    sr.write("entry", entry.address)
    await token_trustlist.add_trusted(entry.address);

    console.log("adding assets...")

    data = web3.eth.abi.encodeFunctionCall({
          name: 'get_asset_price',
          type: 'function',
          inputs: [{"name": "addr", "type": "address"}]
          }, ["0x5f4eC3Df9cbd43714FE2740f5E3616155c5b8419"]);
    await vtrade.add_asset("pETH", cloracletype.address, data);
    console.log(data);

    data = web3.eth.abi.encodeFunctionCall({
        name: 'get_asset_price',
        type: 'function',
        inputs: [{"name": "addr", "type": "address"}]
        }, ["0xF4030086522a5bEEa4988F8cA5B36dbC97BeE88c"]);
    await vtrade.add_asset("pBTC", cloracletype.address, data);
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
