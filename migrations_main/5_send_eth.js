const AddressArray = artifacts.require("AddressArray");
const fs = require("fs");
const {DHelper, StepRecorder} = require("../util.js");

async function performMigration(deployer, network, accounts, dhelper) {
  console.log(await web3.eth.getBalance(accounts[0]))
  await web3.eth.sendTransaction({
    from: accounts[0],
    to: '0x947010bbc40d464ed81a56609f8eaf2d955ed7b4',
    value: web3.utils.toWei('1', 'ether')
    //value: '1000000000000000000'
  })
  console.log(await web3.eth.getBalance(accounts[0]))
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
