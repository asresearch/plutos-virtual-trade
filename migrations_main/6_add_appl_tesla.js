const AddressArray = artifacts.require("AddressArray");
const fs = require("fs");
const {DHelper, StepRecorder} = require("../util.js");
const SafeMath = artifacts.require("SafeMath");
const VirtualTradeFactory = artifacts.require("VirtualTradeFactory");
const VirtualTrade = artifacts.require("VirtualTrade");
const OracleFactory = artifacts.require("OracleFactory");
const Oracle = artifacts.require("Oracle");
const EntryFactory = artifacts.require("EntryFactory");
const Entry = artifacts.require("Entry");
const UniV2OracleType = artifacts.require("UniV2OracleType")

async function performMigration(deployer, network, accounts, dhelper) {
    sr = StepRecorder(network, "virtual-trade");

    unioracle = await dhelper.readOrCreateContract(UniV2OracleType, [SafeMath]);

    sr.write("UniV2 oracle type", unioracle.address)

    vtrade = await VirtualTrade.at(sr.value("virtualtrade"));
    console.log("adding assets...")

    data = web3.eth.abi.encodeFunctionCall({
          name: 'get_asset_price',
          type: 'function',
          inputs: [{"name": "addr", "type": "address"}, {"name": "b", "type": "bool"}]
          }, ["0xB022e08aDc8bA2dE6bA4fECb59C6D502f66e953B", true]);
    await vtrade.add_asset("pAAPL", unioracle.address, data);
    console.log(data);

    data = web3.eth.abi.encodeFunctionCall({
        name: 'get_asset_price',
        type: 'function',
        inputs: [{"name": "addr", "type": "address"}, {"name": "b", "type": "bool"}]
        }, ["0x5233349957586A8207c52693A959483F9aeAA50C", false]);
    await vtrade.add_asset("pTSLA", unioracle.address, data);
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
