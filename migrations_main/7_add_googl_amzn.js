const AddressArray = artifacts.require("AddressArray");
const fs = require("fs");
const {DHelper, StepRecorder} = require("../util.js");
const SafeMath = artifacts.require("SafeMath");
const VirtualTradeFactory = artifacts.require("VirtualTradeFactory");
const VirtualTrade = artifacts.require("VirtualTrade");
const OracleFactory = artifacts.require("OracleFactory");
const Oracle = artifacts.require("Oracle");
const EntryFactory = artifacts.require("EntryFactory");
const Entry = artifacts.require("Entry");
const UniV2OracleType = artifacts.require("UniV2OracleType")

async function performMigration(deployer, network, accounts, dhelper) {
    sr = StepRecorder(network, "virtual-trade");

    unioracle = sr.value("UniV2 oracle type")

    vtrade = await VirtualTrade.at(sr.value("virtualtrade"));
    console.log("adding assets...")

    data = web3.eth.abi.encodeFunctionCall({
          name: 'get_asset_price',
          type: 'function',
          inputs: [{"name": "addr", "type": "address"}, {"name": "b", "type": "bool"}]
          }, ["0x0Ae8cB1f57e3b1b7f4f5048743710084AA69E796", false]);
    await vtrade.add_asset("pAMZN", unioracle, data);
    console.log(data);

    data = web3.eth.abi.encodeFunctionCall({
        name: 'get_asset_price',
        type: 'function',
        inputs: [{"name": "addr", "type": "address"}, {"name": "b", "type": "bool"}]
        }, ["0x4b70ccD1Cf9905BE1FaEd025EADbD3Ab124efe9a", false]);
    await vtrade.add_asset("pGOOGL", unioracle, data);
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
