const AddressArray = artifacts.require("AddressArray");
const fs = require("fs");
const {DHelper, StepRecorder} = require("../util.js");
const SafeMath = artifacts.require("SafeMath");
const VirtualTradeFactory = artifacts.require("VirtualTradeFactory");
const VirtualTrade = artifacts.require("VirtualTrade");
const OracleFactory = artifacts.require("OracleFactory");
const Oracle = artifacts.require("Oracle");
const EntryFactory = artifacts.require("EntryFactory");
const Entry = artifacts.require("Entry");
const UniV2OracleType = artifacts.require("UniV2OracleType")

async function performMigration(deployer, network, accounts, dhelper) {
    sr = StepRecorder(network, "virtual-trade");

    cloracle = sr.value("chainlink oracle type")

    vtrade = await VirtualTrade.at(sr.value("virtualtrade"));
    console.log("adding assets...")

    data = web3.eth.abi.encodeFunctionCall({
          name: 'get_asset_price',
          type: 'function',
          inputs: [{"name": "addr", "type": "address"}]
        }, ["0x214eD9Da11D2fbe465a6fc601a91E62EbEc1a0D6"]);
    await vtrade.add_asset("pXAU", cloracle, data);
    console.log(data);

    data = web3.eth.abi.encodeFunctionCall({
        name: 'get_asset_price',
        type: 'function',
        inputs: [{"name": "addr", "type": "address"}]
      }, ["0xf3584F4dd3b467e73C2339EfD008665a70A4185c"]);
    await vtrade.add_asset("pWTI", cloracle, data);
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
