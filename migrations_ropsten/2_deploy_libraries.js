const SafeMath = artifacts.require("SafeMath");
const SafeERC20 = artifacts.require("SafeERC20");
const AddressArray = artifacts.require("AddressArray");

const {DHelper} = require("./util.js");

async function performMigration(deployer, network, accounts, dhelper) {
  console.log("dhelper", dhelper)
  await dhelper.readOrCreateContract(SafeMath)
  await dhelper.readOrCreateContract(SafeERC20);
  await dhelper.readOrCreateContract(AddressArray)
  //await deployer.deploy(SafeMath);
  //await deployer.deploy(AddressArray);
  //await deployer.deploy(SafeERC20);
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};