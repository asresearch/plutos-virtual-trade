const AddressArray = artifacts.require("AddressArray");
const fs = require("fs");
const {DHelper, StepRecorder} = require("./util.js");
const SafeMath = artifacts.require("SafeMath");
const SafeERC20 = artifacts.require("SafeERC20");
const TrustListFactory = artifacts.require("TrustListFactory");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const VirtualTradeFactory = artifacts.require("VirtualTradeFactory");
const OracleFactory = artifacts.require("OracleFactory");
const EntryFactory = artifacts.require("EntryFactory");
const MockOracleTypeFactory = artifacts.require("MockOracleTypeFactory")



async function performMigration(deployer, network, accounts, dhelper) {
  sr = StepRecorder(network, "virtual-trade");

  aa = await AddressArray.deployed()
  sr.write("AddressArray", aa.address)

  sm = await SafeMath.deployed()
  sr.write("SafeMath", sm.address)

  console.log("checking factories...")
  tlist_factory = await dhelper.readOrCreateContract(TrustListFactory, [AddressArray]);
  token_factory = await dhelper.readOrCreateContract(ERC20TokenFactory, [AddressArray]);
  oracle_factory = await dhelper.readOrCreateContract(OracleFactory);
  vtfactory = await dhelper.readOrCreateContract(VirtualTradeFactory, [SafeMath]);
  entry_factory = await dhelper.readOrCreateContract(EntryFactory, [SafeMath], [SafeERC20]);
  mockoracle_factory = await dhelper.readOrCreateContract(MockOracleTypeFactory);


  /*
  console.log("creating trust list...")
  tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
  token_trustlist = await TrustList.at(tx.logs[0].args.addr);
  sr.write("token_tl", token_trustlist.address);
  token_trustlist.add_trusted(accounts[0])

  tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
  pool_trustlist = await TrustList.at(tx.logs[0].args.addr);
  sr.write("pool_tl", token_trustlist.address);
  pool_trustlist.add_trusted(accounts[0])

  console.log("creating payment pool...")
  tx = await paypool_factory.newPaymentPool('testPP', pool_trustlist.address);
  paypool = await PaymentPool.at(tx.logs[0].args.addr)
  sr.write("paypool", paypool.address);
  
  console.log("creating token...")
  tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000', 0, "DSToken", 18, "DST", true, token_trustlist.address, paypool.address);
  token = await PERCToken.at(tx.logs[0].args._cloneToken);
  sr.write("DStoken", token.address);*/

}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
