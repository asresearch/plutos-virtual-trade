const AddressArray = artifacts.require("AddressArray");
const fs = require("fs");
const {DHelper, StepRecorder} = require("./util.js");
const SafeMath = artifacts.require("SafeMath");
const SafeERC20 = artifacts.require("SafeERC20");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const VirtualTradeFactory = artifacts.require("VirtualTradeFactory");
const VirtualTrade = artifacts.require("VirtualTrade");
const OracleFactory = artifacts.require("OracleFactory");
const Oracle = artifacts.require("Oracle");
const EntryFactory = artifacts.require("EntryFactory");
const Entry = artifacts.require("Entry");
const MockOracleTypeFactory = artifacts.require("MockOracleTypeFactory")
const MockOracleType = artifacts.require("MockOracleType");



async function performMigration(deployer, network, accounts, dhelper) {
    sr = StepRecorder(network, "virtual-trade");

    console.log("creating trustlist...")
    tlist_factory = await TrustListFactory.deployed();
    tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
    token_trustlist = await TrustList.at(tx.logs[0].args.addr);
    token_trustlist.add_trusted(accounts[0]) //change this in main
    sr.write("chip_tl", token_trustlist.address)

    tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
    oracle_trustlist = await TrustList.at(tx.logs[0].args.addr);
    sr.write("oracle_tl", oracle_trustlist.address)

    pusd = "0x59a9bda10d60dD893eE1089AE0782B41BE1b1652"

    console.log("creating chip...")

    token_factory = await ERC20TokenFactory.deployed();
    tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000', 0, "plutos_chip", 18, "CHIP", true);
    chip = await ERC20Token.at(tx.logs[0].args._cloneToken);
    sr.write("chip", chip.address)
    await chip.changeTrustList(token_trustlist.address);

    if (network.includes("ropsten")){
      mockoracletype = await MockOracleType.at("0x302A359e8c8429B01239DAb4d41875895788BA09")
    }
    else{
      mockoracle_factory = await MockOracleTypeFactory.deployed();
      tx = await mockoracle_factory.newMockOracleType();
      mockoracletype = await MockOracleType.at(tx.logs[0].args.addr);
      sr.write("mockoracletype", mockoracletype.address)
      s = "1000000000000000000"
      await mockoracletype.add_or_set_asset("TESTAA", s);
      await mockoracletype.add_or_set_asset("TESTBB", "8000000000000000000");
      await mockoracletype.add_or_set_asset("BTC", "40000000000000000000000");
      await mockoracletype.add_or_set_asset("ETH", "3000000000000000000000");
    }
    /*if (network.includes("ropsten")){
      oracle = await Oracle.at("0xda980D1E2C78e482d864804cE1c3F6177c4C261B")
    }
    else{*/
    console.log("creating oracle...")

    oracle_factory = await OracleFactory.deployed();
    tx = await oracle_factory.newOracle();
    oracle = await Oracle.at(tx.logs[0].args.addr);    
    sr.write("oracle", oracle.address)
    await oracle.changeTrustList(oracle_trustlist.address);
    //}

    console.log("creating virtual trade...")
    vt_factory = await VirtualTradeFactory.deployed();
    tx = await vt_factory.newVirtualTrade(chip.address, oracle.address);
    vtrade = await VirtualTrade.at(tx.logs[0].args.addr);
    sr.write("virtualtrade", vtrade.address);
    await oracle_trustlist.add_trusted(vtrade.address)
    await token_trustlist.add_trusted(vtrade.address);

    console.log("creating entry...")

    entry_factory = await EntryFactory.deployed();
    tx = await entry_factory.newEntry(pusd, chip.address, vtrade.address);
    entry = await Entry.at(tx.logs[0].args.addr);
    sr.write("entry", entry.address)
    await token_trustlist.add_trusted(entry.address);

    console.log("adding assets...")

    data = web3.eth.abi.encodeFunctionCall({
          name: 'get_asset_price',
          type: 'function',
          inputs: [{"name": "_name", "type": "string"}]
          }, ["TESTAA"]);
    await vtrade.add_asset("TESTAA", mockoracletype.address, data);

    data = web3.eth.abi.encodeFunctionCall({
        name: 'get_asset_price',
        type: 'function',
        inputs: [{"name": "_name", "type": "string"}]
        }, ["TESTBB"]);
    await vtrade.add_asset("TESTBB", mockoracletype.address, data);

    data = web3.eth.abi.encodeFunctionCall({
      name: 'get_asset_price',
      type: 'function',
      inputs: [{"name": "_name", "type": "string"}]
      }, ["BTC"]);
    await vtrade.add_asset("BTC", mockoracletype.address, data);

    data = web3.eth.abi.encodeFunctionCall({
      name: 'get_asset_price',
      type: 'function',
      inputs: [{"name": "_name", "type": "string"}]
      }, ["ETH"]);
    await vtrade.add_asset("ETH", mockoracletype.address, data);
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
