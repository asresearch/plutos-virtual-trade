const AddressArray = artifacts.require("AddressArray");
const fs = require("fs");
const {DHelper, StepRecorder} = require("../util.js");
const MockOracleType = artifacts.require("MockOracleType");

async function performMigration(deployer, network, accounts, dhelper) {
  mockoracletype = await MockOracleType.at("0x302A359e8c8429B01239DAb4d41875895788BA09")
  mockoracletype.transferOwnership("0x7324237968A6Bf468331Abe639235F6683214DE5")
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
