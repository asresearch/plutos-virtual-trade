const { expect } = require('chai');
const { BN, constants, expectEvent, expectRevert  } = require('openzeppelin-test-helpers');
const { keccak256, bufferToHex, toBuffer } = require('ethereumjs-util');
//const web3 = require("web3")

//const getBlockNumber = require('./blockNumber')(web3)
const SafeMath = artifacts.require("SafeMath");
const SafeERC20 = artifacts.require("SafeERC20");
const TrustListFactory = artifacts.require("TrustListFactory");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const VirtualTradeFactory = artifacts.require("VirtualTradeFactory");
const OracleFactory = artifacts.require("OracleFactory");
const Oracle = artifacts.require("Oracle");
const EntryFactory = artifacts.require("OracleFactory");
const MockOracleTypeFactory = artifacts.require("MockOracleTypeFactory")
const MockOracleType = artifacts.require("MockOracleType");

contract("TestLockInit", (accounts) =>{
    context("init", async ()=>{
        let mockoracletype = {}
        let oracle = {}
     
        it("Initial", async function(){
            mockoracle_factory = await MockOracleTypeFactory.deployed();
            tx = await mockoracle_factory.newMockOracleType();
            mockoracletype = await MockOracleType.at(tx.logs[0].args.addr);

            oracle_factory = await OracleFactory.deployed();
            tx = await oracle_factory.newOracle();
            oracle = await Oracle.at(tx.logs[0].args.addr);    
        })

        it("test oracle", async ()=>{
            s = "12345678900987654321"
            await mockoracletype.add_or_set_asset("TESTAA", s);
            await mockoracletype.add_or_set_asset("TESTBB", "1111111111111111111");
            await mockoracletype.add_or_set_asset("TESTBB", "1123123123123123123");
            data = web3.eth.abi.encodeFunctionCall({
                name: 'get_asset_price',
                type: 'function',
                inputs: [{"name": "_name", "type": "string"}]
              }, ["TESTAA"]);
            await oracle.add_asset("TESTAA", mockoracletype.address, data);

            //p1 = (await oracle.get_asset_price("TESTAA")).toString();

            var contract1 = new web3.eth.Contract(oracle.abi, oracle.address)
            p1 = await contract1.methods.get_asset_price("TESTAA").call({from:accounts[0]})
            console.log("TESTAA:", p1);
            expect(p1).to.equal(s);
            await expectRevert(oracle.get_asset_price("TESTBB"), "Invalid Asset")

            data = web3.eth.abi.encodeFunctionCall({
                name: 'get_asset_price',
                type: 'function',
                inputs: [{"name": "_name", "type": "string"}]
              }, ["TESTBB"]);
            await oracle.add_asset("TESTBB", mockoracletype.address, data);

            var contract1 = new web3.eth.Contract(oracle.abi, oracle.address)
            p1 = await contract1.methods.get_asset_price("TESTBB").call({from:accounts[0]})
            console.log("TESTBB:", p1);
            expect(p1).to.equal("1123123123123123123");
        })
    })
})