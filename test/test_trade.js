const { expect } = require('chai');
const { BN, constants, expectEvent, expectRevert  } = require('openzeppelin-test-helpers');
const { keccak256, bufferToHex, toBuffer } = require('ethereumjs-util');
//const web3 = require("web3")

//const getBlockNumber = require('./blockNumber')(web3)
const SafeMath = artifacts.require("SafeMath");
const SafeERC20 = artifacts.require("SafeERC20");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const VirtualTradeFactory = artifacts.require("VirtualTradeFactory");
const VirtualTrade = artifacts.require("VirtualTrade");
const OracleFactory = artifacts.require("OracleFactory");
const Oracle = artifacts.require("Oracle");
const EntryFactory = artifacts.require("EntryFactory");
const Entry = artifacts.require("Entry");
const MockOracleTypeFactory = artifacts.require("MockOracleTypeFactory")
const MockOracleType = artifacts.require("MockOracleType");

contract("TestLockInit", (accounts) =>{
    context("init", async ()=>{
        let mockoracletype = {}
        let oracle = {}
        let pusd = {}
        let chip = {}
        let vtrade = {}
        let entry = {}
        it("Initial", async function(){
            tlist_factory = await TrustListFactory.deployed();
            tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
            token_trustlist = await TrustList.at(tx.logs[0].args.addr);
            token_trustlist.add_trusted(accounts[0])

            token_factory = await ERC20TokenFactory.deployed();
            tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000', 0, "PUSD", 18, "PUSD", true);
            pusd = await ERC20Token.at(tx.logs[0].args._cloneToken);
            await pusd.changeTrustList(token_trustlist.address)

            tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000', 0, "CHIP", 18, "CHIP", true);
            chip = await ERC20Token.at(tx.logs[0].args._cloneToken);
            await chip.changeTrustList(token_trustlist.address)


            mockoracle_factory = await MockOracleTypeFactory.deployed();
            tx = await mockoracle_factory.newMockOracleType();
            mockoracletype = await MockOracleType.at(tx.logs[0].args.addr);

            oracle_factory = await OracleFactory.deployed();
            tx = await oracle_factory.newOracle();
            oracle = await Oracle.at(tx.logs[0].args.addr);    
            await oracle.changeTrustList(token_trustlist.address);
     

            vt_factory = await VirtualTradeFactory.deployed();
            tx = await vt_factory.newVirtualTrade(chip.address, oracle.address);
            vtrade = await VirtualTrade.at(tx.logs[0].args.addr);
    
            await token_trustlist.add_trusted(vtrade.address);


            entry_factory = await EntryFactory.deployed();
            tx = await entry_factory.newEntry(pusd.address, chip.address, vtrade.address);
            entry = await Entry.at(tx.logs[0].args.addr);
            await token_trustlist.add_trusted(entry.address);
            entry.change_fee_pool(accounts[9])

            await pusd.generateTokens(accounts[0], 10000);
            await pusd.generateTokens(accounts[1], 10000);
            await pusd.generateTokens(accounts[2], 10000);
            await pusd.generateTokens(accounts[3], 10000);


            await pusd.approve(entry.address, 20000000000, {from:accounts[0]});
            await pusd.approve(entry.address, 20000000000, {from:accounts[1]});
            await pusd.approve(entry.address, 20000000000, {from:accounts[2]});
            await pusd.approve(entry.address, 20000000000, {from:accounts[3]});


        })

        it("test oracle", async ()=>{
            await entry.deposit(10000, {from:accounts[0]})
            await entry.deposit(10000, {from:accounts[1]})
            b0 = (await chip.balanceOf(accounts[0])).toNumber();
            expect(b0).to.equal(10000);
            b0 = (await chip.balanceOf(accounts[1])).toNumber();
            expect(b0).to.equal(10000);

            s = "1000000000000000000"
            await mockoracletype.add_or_set_asset("TESTAA", s);
            await mockoracletype.add_or_set_asset("TESTBB", "8000000000000000000");
            data = web3.eth.abi.encodeFunctionCall({
                name: 'get_asset_price',
                type: 'function',
                inputs: [{"name": "_name", "type": "string"}]
              }, ["TESTAA"]);
            await vtrade.add_asset("TESTAA", mockoracletype.address, data);

            data = web3.eth.abi.encodeFunctionCall({
                name: 'get_asset_price',
                type: 'function',
                inputs: [{"name": "_name", "type": "string"}]
              }, ["TESTBB"]);
            await vtrade.add_asset("TESTBB", mockoracletype.address, data);

            b0 = (await vtrade.get_total_chip()).toNumber();
            expect(b0).to.equal(20000);
            await vtrade.buyin("TESTAA", 10000, 0, accounts[0], {from:accounts[0]})
            await vtrade.buyin("TESTBB", 10000, 0, accounts[1], {from:accounts[1]})
            b0 = (await vtrade.get_total_chip()).toNumber();
            expect(b0).to.equal(20000);
            await mockoracletype.add_or_set_asset("TESTAA", "3000000000000000000");
            await vtrade.sell("TESTAA", 10000, 0, accounts[0])
            b0 = (await chip.balanceOf(accounts[0])).toNumber();
            expect(b0).to.equal(30000);
            await entry.withdraw(30000, {from:accounts[0]});
            b0 = (await pusd.balanceOf(accounts[0])).toNumber();
            expect(b0).to.equal(14700);//300 for fee

            await entry.deposit(5000, {from:accounts[2]});
            b0 = (await vtrade.get_total_chip()).toNumber();
            expect(b0).to.equal(20000);

            await expectRevert(vtrade.buyin("TESTAA", 5000, 0, accounts[2]), "permission denied");
            await mockoracletype.add_or_set_asset("TESTBB", "2000000000000000000");
            await vtrade.set_allow(accounts[0], true, {from:accounts[2]});

            await vtrade.buyin("TESTBB", 5000, 0, accounts[2])
            await mockoracletype.add_or_set_asset("TESTBB", "1000000000000000000");

            await vtrade.sell("TESTBB", 1250, 0, accounts[1], {from:accounts[1]});

            console.log("chip1:", (await chip.balanceOf(accounts[1])).toNumber());
            console.log("chip supply:", (await chip.totalSupply()).toNumber());
            console.log("chip total:", (await vtrade.get_total_chip()).toNumber());

            await entry.withdraw(1250, {from:accounts[1]});
            b0 = (await pusd.balanceOf(accounts[1])).toNumber();
            expect(b0).to.equal(1400); //28 for fee 

            b0 = (await vtrade.get_total_chip()).toNumber();
            expect(b0).to.equal(7500);
        })

        it("test view", async ()=>{
            tb = await vtrade.get_user_position(accounts[2], "TESTBB");
            console.log("tb:", tb.toString());
            await vtrade.sell("TESTBB", 1250, 0 ,accounts[2]);
            tb = await vtrade.get_user_position(accounts[2], "TESTBB");
            expect(tb.toString()).to.equal("1250");
            b0 = (await chip.balanceOf(accounts[2])).toNumber();
            expect(b0).to.equal(6250);
            //error 1 : if b0 = 7500, means you generate full amount chip when selling only part of position 
        })
        it("test buyin with position", async ()=>{
            b0 = (await vtrade.get_total_chip()).toNumber();
            console.log("total chip:", b0)
            b0 = (await pusd.balanceOf(entry.address)).toNumber();
            console.log("total pusd:", b0)

            b0 = (await chip.balanceOf(accounts[0])).toNumber();
            console.log("chip0:", b0)
            b0 = (await chip.balanceOf(accounts[1])).toNumber();
            console.log("chip1:", b0)
            b0 = (await chip.balanceOf(accounts[2])).toNumber();
            console.log("chip2:", b0)

            b0 = (await vtrade.get_user_chip_in_position(accounts[0])).toNumber();
            console.log("pchip0:", b0)
            b0 = (await vtrade.get_user_chip_in_position(accounts[1])).toNumber();
            console.log("pchip1:", b0)
            b0 = (await vtrade.get_user_chip_in_position(accounts[2])).toNumber();
            console.log("pchip2:", b0)

            await entry.deposit(8572, {from:accounts[3]})
            await vtrade.buyinWithPosition("TESTAA", 1000, accounts[3], {from:accounts[3]});
            b0 = (await chip.balanceOf(accounts[3])).toNumber();
            expect(b0).to.equal(4500);
            await mockoracletype.add_or_set_asset("TESTAA", "2000000000000000000");

            await expectRevert(vtrade.sellWithChip("TESTAA", 3000, accounts[3], {from:accounts[3]}), "not enough position")
            await vtrade.sellWithChip("TESTAA", 1000, accounts[3], {from:accounts[3]})
            b0 = (await chip.balanceOf(accounts[3])).toNumber();
            expect(b0).to.equal(5500);
            b0 = (await vtrade.get_user_position(accounts[3], "TESTAA")).toNumber();
            expect(b0).to.equal(500);

        })
    })
})